# Changelog

## 2020-04-23
- updated readme
- updated css
- updated nextpage.html
- updated nopopups.html

## 2020-04-17

### added
- html files
- css files

## 2020-04-16

### added
- readme
- license
- changelog
- html_files.7z
- ccs_files.7z

### changed
- readme
- changelog